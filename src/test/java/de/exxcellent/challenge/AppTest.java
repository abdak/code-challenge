package de.exxcellent.challenge;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Example JUnit 5 test case.
 * @author Benjamin Schmid <benjamin.schmid@exxcellent.de>
 */
class AppTest {


    @Test
    @DisplayName("Should convert nested list to flattened list correctly")
    void convertNestedListToFlattenedListCorrectly() {
        List<Integer> expectedResult = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6));
        List<Integer> result = App.nestedListToFlattenedList(App.EXAMPLE_NESTED_LIST).stream().map(o -> (Integer)o).collect(
            Collectors.toList());

        assertEquals(result, expectedResult);
    }

}